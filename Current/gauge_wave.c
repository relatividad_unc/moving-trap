    x = xi + (FLOAT)grid_ind1*one_dN1;
    t = (*derivs_ptr).time; 
    
    dK  = globals.dfields.du[KK_X][grid_ind1];
    dAx = globals.dfields.du[AX_X][grid_ind1];
    
    a   = (*fields_ptr).u[AL][grid_ind1];
    Ax  = (*fields_ptr).u[AX][grid_ind1];
    g   = (*fields_ptr).u[GG][grid_ind1];
    K   = (*fields_ptr).u[KK][grid_ind1];
    d   = (*fields_ptr).u[DD][grid_ind1];
    
    
 
    
AP = A*PI; 



cosphi = cos(PI*(x-t));
sinphi = sin(PI*(x-t));
pisinphi = PI*sinphi;
Ac = AP*cosphi;
As = AP*pisinphi;

a_h = exp(A*sinphi/2.);

#ifndef COMPLICATED


a_t = -Ac*a - K + Ac*g/2./a_h;

Ax_t = - dK/a_h + Ac*K/2./a_h -(Ac*Ac + As)*g/2./a_h/a_h + Ac*d/2./a_h/a_h - Ac*Ax/2. + As*a/2./a_h;

g_t = -a_h*Ac*a - 2.*a_h*K;

K_t = -a_h*dAx - a_h*Ac*Ax/2. - (-2.*As+Ac*Ac)*a/4. -Ac*K + Ac*d/a_h/4.;

d_t = a_h*(As-Ac*Ac)*a - a_h*Ac*K - a_h*a_h*Ac*Ax - 2.*a_h*dK;
 
#else

a_t = -Ac*a - K + Ac*g/2./a_h;

Ax_t = - dK/a_h + Ac*K/2./a_h -(Ac*Ac + As)*g/2./a_h/a_h + Ac*d/2./a_h/a_h - Ac*Ax/2. + As*a/2./a_h;

g_t = -a_h*Ac*a - 2.*a_h*K;

K_t = -a_h*dAx - a_h*Ac*Ax/2. - (-2.*As+Ac*Ac)*a/4. -Ac*K + Ac*d/a_h/4.;

d_t = a_h*(As-Ac*Ac)*a - a_h*Ac*K - a_h*a_h*Ac*Ax - 2.*a_h*dK;

#endif
