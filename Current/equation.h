#ifndef __CPU_RICCI_H
#define __CPU_RICCI_H

#include "first_macro_1d.h"     /* Where global parameters are defined */
#include "structs_1d.h"         /* Where structures are defined */


struct FUNCTION_PAR { 
  struct GRID_PAR *grid_ptr;
  FLOAT mm;      /* mass of field */
  FLOAT sigma;   /* dissipation parameter */
  FLOAT R10;     /* Boundary condition at x=0, m = R0*p */
  FLOAT R11;     /* Boundary condition at x=1, p = R1*m */ 
  FLOAT c;
  FLOAT s; 
  FLOAT a; 




  /* normals to fases, first digit = coord. second = value */

  FLOAT nx_10;
  FLOAT nx_11;

};



#ifdef IMEX

void FF(struct GRID_PAR *grid_ptr, struct field_array *fields_ptr, struct field_array *derivs_ptr, struct FUNCTION_PAR *function_par);
void FS(struct GRID_PAR *grid_ptr, struct field_array *fields_ptr, struct field_array *derivs_ptr, struct FUNCTION_PAR *function_par);
void FI(struct GRID_PAR *grid_ptr, struct field_array *fields_ptr, struct field_array *derivs_ptr, struct FUNCTION_PAR *function_par);

#else

void FF(struct GRID_PAR *grid_ptr, struct field_array *fields_ptr, struct field_array *derivs_ptr, struct FUNCTION_PAR *function_par);

#endif // IMEX


static inline FLOAT Fx(FLOAT *u, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par, int i);
static inline FLOAT mysign_zero(double d);

static inline FLOAT mymin_3(FLOAT a, FLOAT b, FLOAT c);
static inline FLOAT mymax_3(FLOAT a, FLOAT b, FLOAT c);
static inline FLOAT mymin_4(FLOAT a, FLOAT b, FLOAT c, FLOAT d);
static inline FLOAT mymax_5(FLOAT a, FLOAT b, FLOAT c, FLOAT d, FLOAT e);
static inline FLOAT mymax_7(FLOAT a0, FLOAT a1, FLOAT a2, FLOAT a3, FLOAT a4, FLOAT a5, FLOAT a6);

static inline FLOAT MM3(double a, double b, double c, double weight);
static inline FLOAT DMM(FLOAT a, FLOAT b);
static inline FLOAT DM4(FLOAT a, FLOAT b, FLOAT c, FLOAT d);
static inline FLOAT MP5(FLOAT F0, FLOAT F1, FLOAT F2, FLOAT F3, FLOAT F4);

static inline FLOAT Speed_max(FLOAT *u, struct FUNCTION_PAR *function_par);


#ifdef SOURCE
static inline FLOAT Source(FLOAT *u, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par, int i, double dx);
#endif

#ifdef PARABOLIC
static inline FLOAT  Px(double *u, double *up, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par, double dx);
static inline FLOAT FTurbx(FLOAT *u_p2, FLOAT *u_p, FLOAT *u, FLOAT *u_m, FLOAT *u_m2, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par, int i, FLOAT dx);
#endif







#endif /* __CPU_RICCI__H */
