/*********************************************************************
* Here are the global definitions used by all the subroutines        *
*                                                                    *       
*********************************************************************/
 
#ifdef FIRST_MACRO_1D_H
#else 
#define FIRST_MACRO_1D_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>
/* #include <curses.h> */
/* #include <ncurses.h> */
#include <time.h>       
#include <string.h>    /* functions to get data via web (stadin) */
/* #include <rfftw.h>  */    /*  needed for taking fft in derivs and main */ 

//#define DEBUG
//#define DEBUG_ADISCO
//#define DEBUG_RKC
//#define DEBUG_PYGRAPH


#define DISSIPATION
//#undef DISSIPATION  
 
#define WAVE // the second order wave equation (similar to Schwarzschild)

 
//#define ADV // just the advection equation to see whether it works.

//#define CONSERV // conservative version u_t = D_x (A * u)

//#define DYN // in this case the A field is propagated at fix velocity using A_t = v D_x A and is given as an initial data.


//#define SHARP


//#define SIN // two types of A: A(x) = sin(PI*2.*(x-x_m)/Dx)  Derivative is not smooth on boundary!
//#define POLY_1 // otherwise is A(x) = pow((x - x_m - Dx/2.),4.)*pow((x - x_m + Dx/2.),4.)  * (x - x_m) * 5. / pow(Dx/2.,9.) Derivative is smooth
//#define POLY_3 // with a cubic instead of linear
//#define GAL_T // galilean trasnformatio to leave the trap at rest. 

#define PERIODIC

//#define F_FLUX
//#define NO_LIMITER
//#define K_T
//#define MP5_
//#define NO_LIMITER // takes out the limiter part in MP5  

#ifndef F_FLUX
#define F_DIFF // Finite DIFFERENCES SBP with Penalties or Periodic
#endif


/* ----------------> The number of fields <------------------------ */

/* That is the number of variables that enter the system            */

#ifdef ADV
#define N_FIELDS 1
#define U   0

#ifdef DISSIPATION 
//printf("dissipation not implemented!"); exit(0);
#define N_DERIVS 3
 
#define U_X  0   
#define UA_X 1 
#define DISS_U  2  

#else 
#define N_DERIVS 2

#define U_X 0  
#define UA_X 1

#endif

/* That is the number of gridpoints times the number of fields      */

#define N_TOTAL ((N_GRIDPOINTS_1)*(N_FIELDS))

#define N_AUX 2
#define A_0   0
#define A_X   1


/* The total number of plots (used in struct plot) */

#define N_PLOTS 1

#endif // ADV

#ifdef WAVE
#define N_FIELDS 3
#define PHI   0
#define FU0   1
#define FD1   2


#ifdef DISSIPATION 
//printf("dissipation not implemented!"); exit(0);
#define N_DERIVS 4
 
#define FU1_X  0   
#define FD0_X  1 
#define DISS_FU0  2
#define DISS_FD1  3

#else 
#define N_DERIVS 2

#define FU1_X 0  
#define FD0_X 1

#endif

/* That is the number of gridpoints times the number of fields      */

#define N_TOTAL ((N_GRIDPOINTS_1)*(N_FIELDS))

#define N_AUX 3
#define FU1   0
#define FD0   1
#define HH    2


/* The total number of plots (used in struct plot) */

#define N_PLOTS 3

#endif // WAVE



#ifdef DYN


#define N_FIELDS 2
#define U   0
#define A   1

#ifdef DISSIPATION 
//printf("dissipation not implemented!"); exit(0);
#define N_DERIVS 5
 
#define DISS_U 0   
#define U_X 1 
#define DISS_A  2  
#define A_X 3
#define UA_X 4


#else 
#define N_DERIVS 3

#define U_X 0  
#define A_X 1
#define UA_X 2

#endif

/* That is the number of gridpoints times the number of fields      */

#define N_TOTAL ((N_GRIDPOINTS_1)*(N_FIELDS))

#define N_AUX 2

#define UA 0
#define MM 1 // needed in KT_


/* The total number of plots (used in struct plot) */

#define N_PLOTS 2

#endif



#define FACTOR_1 1  /* Choose it so that the plot is about 20 points */

#ifdef PERIODIC
#define N_GRID_PLOT_PTS_1 (((N_GRIDPOINTS_1) / FACTOR_1))
#undef NO_LAST_POINT
#else
	#ifdef NO_LAST_POINT
	        #define N_GRID_PLOT_PTS_1 (((N_GRIDPOINTS_1-1) / FACTOR_1)) //do not plot the last point
	#else
			#define N_GRID_PLOT_PTS_1 (((N_GRIDPOINTS_1-1) / FACTOR_1) +1)
	#endif
#endif

/* The gridpoints for plots, usually a fraction of N_GRIDPOINTS */


#define N_POINTS 1



/* The number PI */

#ifdef PI

#undef PI
#endif
#define PI 3.141592653589793238462643

/* --------------------------------------------------------------*/

/* The size of variables */

#define FLOAT double

/* To print the values of some macros */
/* Warning: only use where OUT_PUT is properly defined */

#define Strg(x) #x
#define PRINT_MACRO_VALUE(x) fprintf(OUT_PUT,"<li> Using %s = %s  </br>\n",#x,Strg(x));
#define GET_MACRO_VALUE(x)   sprintf(macro_value_strg,"%s",Strg(x))


/* -------------------- FUNCTIONS -------------------------------*/

/* different types of plotting routines, could be:
   graph(er-3d), map(le)3, map(le)5, mtv, idl, sv, fsv or gen 
   this are changes on the type of file where 
   the routine is defined */

#define SDF  /* use to change the plot structure */
#undef SDF
#undef SV
#undef NETCDF
#define ASCHI
#define PYGRAPH

#ifdef SDF
#include "sdf.h"
#define ADISCO adisco_sdf_1d
//#define ADISCO_POINT adisco_txt_point_1d
	//#else
	//#define ADISCO adisco_dummy_1d
#endif

#ifdef ASCHI
#define ADISCO adisco_aschi_1d
#endif

#ifdef NETCDF
#define ADISCO adisco_netcdf_1d
#endif

#ifdef SV
#define ADISCO adisco_fsv_1d   /* #define ADISCO adisco_sv_3d */
#endif



/* -----------------------------------------------------------------*/


/* input output definitions */

#undef WEB_INPUT              /* define only one of these two */
#define FILE_INPUT

#ifdef WEB_INPUT
#define OUT_PUT stdout
#endif
#ifdef FILE_INPUT
#define OUT_PUT file_data_ptr
#endif

/* used in routines to get data from the web */
#define MAX_ENTRIES 400
#define MAX_ENTRY 200


#define MAIL  /* To send e-mails with output data or saying the run finished */
#undef MAIL


/* ----------------------- structures -------------------------*/

#define GRID_PAR grid_1d         /* grid parameters */
#define PLOT_PAR plot_1d         /* ploting parameters */
#define INI_PAR gw_ini_par_1d  /* where initial data parameters are stored */
#define FUNCTION_PAR gw_par_1d /* equation parameters */


/* other structures */

#ifdef WEB_INPUT
#define INPUT_FUNCTION wave_1d_input_web
#endif
#ifdef FILE_INPUT
#define INPUT_FUNCTION gw_1d_input_file
#endif


/* -----------------------------------------------------------------*/

/* different functions for the integrator, here goes
   most of the physical input */

#ifdef IMEX 
#define FF gw_eq_F              /* Non stif part of RHS */
#define FS gw_eq_S              /* Stiff part of RHS */
#define FI gw_eq_I              /* Inversion for implicit part */
#else
#define FF gw_eq
#endif // IMEX

/* different arguments for the function FF */


#define FF_ARG struct grid_1d *grid_1d, struct field_array  *fields, struct field_array  *derivs, struct FUNCTION_PAR *function_par
#define FF_ARG_DUMMY struct GRID_PAR *, struct field_array  *, struct field_array  *, struct FUNCTION_PAR *

/* different functions to take derivatives derivQ_1d, derivQ_3_1d, derivD_1d, derivQQ_1d, deriv_strand_third_order_boundaries_sixth_interior, deriv_strand_fourth_order_boundaries_eight_interior, etc */

#ifdef PERIODIC

//#define DERIV derivD_Per_1d
#define DERIV deriv_6_Per_1d
//#define DERIV deriv_8_Per_1d

#define INTERNALPRODUCT internalproduct_per_sixth_interior_1d

#else 

//#define DERIV derivS_1d
//#define DERIV deriv_strand_fourth_order_boundaries_eight_interior_1d
#define DERIV deriv_strand_third_order_boundaries_sixth_interior_1d

#define INTERNALPRODUCT internalproduct_third_order_boundaries_sixth_interior_1d
#endif // PERIODIC

#define NON_OPTIMIZED_EIGHT_ORDER_OPERATOR
#undef NON_OPTIMIZED_EIGHT_ORDER_OPERATOR

#define OPTIMIZED_EIGHT_ORDER_OPERATOR
//#undef OPTIMIZED_EIGHT_ORDER_OPERATOR

/* -----------------------------------------------------------------*/

/* different dissipative operators diss_KO_4_D_1d, diss_KO_4_00_D_1d */

#ifdef DISSIPATION
	#ifdef PERIODIC
		//#define DISS diss_KO4_Per_1d
		//#define DISS diss_KO6_Per_1d
		#define DISS diss_KO8_Per_1d
	#else
		//#define DISS diss_KO6_1d
		//#define DISS diss_KO8_1d
	#endif
#endif
/* different runge-kutta routines */

#ifdef IMEX
//#define RKX imexrkL343 // for this one define IMEX
#define RKX imexrkssp3 // for this one define IMEX paralell with tvd3

#else
//#define RKX rk3
#ifdef F_FLUX
#define RKX tvd3  
#else
#define RKX rk4
#endif
#endif




/* -----------------------------------------------------------------*/

#endif
