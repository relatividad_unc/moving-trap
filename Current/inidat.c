/*********************************************************************
*                                                                    *
* inidat -- provides initial data                                    *
*                                                                    *
* Parameters:                                                        *
* y_a_ptr        -- pointer where to write the initial data          *
* initial_time   -- initial time                                     *
*                                                                    *
* Returns: pointer to field_array where data was writen              *
*                                                                    *
*********************************************************************/

#include "first_macro_1d.h"  /* Where global parameters are defined */
#include "structs_1d.h"      /* Where structures are defined */
#include "derivs_1d.h"       /* Where derivatives functions are defined */
#include "inidat.h"


/* struct field_array *inidat(struct field_array *y_a_ptr) { */

void inidat(struct field_array *y_a_ptr, 
	    struct GRID_PAR *grid_1d_ptr,
	    struct INI_PAR *ini_par_ptr){

/* -------> grid parameters <--------------------------*/


  
    int ni_1 = (*grid_1d_ptr).start_grid; 
    int nf_1 = (*grid_1d_ptr).final_grid;

    FLOAT xi = (*grid_1d_ptr).initial_x;
    FLOAT xf = (*grid_1d_ptr).final_x;

/* -------> initial data parameters <------------------*/

#ifdef PERIODIC
 FLOAT twoPIdN1 = 2.*PI*(xf-xi)/(FLOAT)(nf_1-ni_1);
 FLOAT one_dN1 = (xf-xi)/(FLOAT)(nf_1-ni_1);
#else
 FLOAT twoPIdN1 = 2.*PI*(xf-xi)/(FLOAT)(nf_1-ni_1-1);
 FLOAT one_dN1 = (xf-xi)/(FLOAT)(nf_1-ni_1-1);
#endif

  /* Parameters from main */

  FLOAT a0 = (*ini_par_ptr).a0;                
  FLOAT k_a_10 = (*ini_par_ptr).k_a_10; 
  FLOAT shift_a0 = (*ini_par_ptr).shift_a0;          

/* Amplitude sin(k_a1* x + shift_a1) in U1 */

  FLOAT a1 = (*ini_par_ptr).a1;                
  FLOAT k_a_11 = (*ini_par_ptr).k_a_11;  
  FLOAT shift_a1 = (*ini_par_ptr).shift_a1;          

/* Amplitude of cos(k_a0* x + shift_a0) in U0 */

  FLOAT c0 = (*ini_par_ptr).c0;                
  FLOAT k_c_10 = (*ini_par_ptr).k_c_10;  
  FLOAT shift_c0 = (*ini_par_ptr).shift_c0;          

/* Amplitude of cos(k_c1* x + shift_c0) in U1 */
  FLOAT c1 = (*ini_par_ptr).c1;                
  FLOAT k_c_11 = (*ini_par_ptr).k_c_11;
  FLOAT shift_c1 = (*ini_par_ptr).shift_c1;          

/* Amplitude of b0*exp(-((x-c0_1)^2 + (y-c0_2)^)/sigma_b0) in U0 */
  FLOAT b0 = (*ini_par_ptr).b0;                
  FLOAT sigma_b0 = (*ini_par_ptr).sigma_b0;
  FLOAT c0_1 = (*ini_par_ptr).c0_1;


/* Amplitude of exp(cos(k_b1*x)^2/sigma_b1) in U1 */
  FLOAT b1 = (*ini_par_ptr).b1;                
  FLOAT sigma_b1 = (*ini_par_ptr).sigma_b1;
  FLOAT c1_1 = (*ini_par_ptr).c1_1;

/* Global wave motion */

  FLOAT v1 = (*ini_par_ptr).v1;

// function parameters

//FLOAT A = (*(*ini_par_ptr).function_par_ptr).c;



#ifdef DEBUG
  printf("A = %f\n",A);
#endif

/*---------> values for different fields <-------------*/

/* struct field_array y; */

/* first the time */

/* y.a.time = initial_time; */

(*y_a_ptr).time = (*grid_1d_ptr).initial_time;

		// PHI 

 


printf("initialdatatype = %d\n", (int)ini_par_ptr->initial_data_type);
switch((int)ini_par_ptr->initial_data_type){
  case 0: 
  {FLOAT Dx = (xf - xi)/ 10.; // With of A initial
	FLOAT v = 0.0;
	FLOAT x_m = (*(*ini_par_ptr).function_par_ptr).c; // Center of A initial 
	#ifdef GAL_T
	v = (*(*ini_par_ptr).function_par_ptr).s; // velocity for galiean transformation

	#endif
   
 register int g_ind1; 
 FLOAT x;
 FLOAT xm = Dx/2./sqrt(5.);
 FLOAT H0 = 2. * pow((xm - Dx/2.),4.)*pow((xm + Dx/2.),4.) * pow(xm,2.);

  for (g_ind1 = ni_1; g_ind1 < nf_1; ++g_ind1) {

    x= xi + (FLOAT)g_ind1*one_dN1;
    
#ifdef ADV

        //(*y_a_ptr).u[U][g_ind1] = exp(-x*x/(0.1*0.1));
        (*y_a_ptr).u[U][g_ind1] = fabs(x) > 0.5 ? 0.0 : pow((x-0.5)*(x+0.5),8) * 65536.0; //normalization factor = 2**16
#endif
 
#ifdef WAVE
        FLOAT H = fabs(x - x_m) - Dx/2. > 0. ? 0. : pow((x - x_m - Dx/2.),4.)*pow((x - x_m + Dx/2.),4.) * pow((x - x_m),2.) / H0;
        //(*y_a_ptr).u[FU0][g_ind1] = 0.0; //H; //0.0;
        (*y_a_ptr).u[PHI][g_ind1] = fabs(x) > 0.5 ? 0.0 : pow((x-0.5)*(x+0.5),8) * 65536.0; //normalization factor = 2**16
        (*y_a_ptr).u[FD1][g_ind1] = fabs(x) > 0.5 ? 0.0 : 16. * x * pow((x-0.5)*(x+0.5),7) * 65536.0; //normalization factor = 2**16
        (*y_a_ptr).u[FU0][g_ind1] = -(1. + 2. * H) * (*y_a_ptr).u[FD1][g_ind1];
        (*y_a_ptr).u[FU0][g_ind1] = (*y_a_ptr).u[FU0][g_ind1] + ((fabs(x) > 0.5) ? 0.0 : 32. * H * fabs(x-x_m) * pow((x-0.5)*(x+0.5),7) * 65536.0); //normalization factor = 2**16
#endif

#ifdef DYN
        (*y_a_ptr).u[U][g_ind1] = fabs(x) > 0.5 ? 0.0 : pow((x-0.5)*(x+0.5),8) * 65536.0; //normalization factor = 2**16
#endif
#ifdef SIN
	 (*y_a_ptr).u[A][g_ind1] = fabs(x - x_m) - Dx/2. > 0. ? 0. : sin(2.*PI*(x-x_m)/ Dx) + v;
#endif
#ifdef POLY_1
	 (*y_a_ptr).u[A][g_ind1] = fabs(x - x_m) - Dx/2. > 0. ? v : pow((x - x_m - Dx/2.),4.)*pow((x - x_m + Dx/2.),4.) 
	 * (x - x_m) * 5. / pow(Dx/2.,9.) + v;
#endif
#ifdef POLY_3
	 (*y_a_ptr).u[A][g_ind1] = fabs(x - x_m) - Dx/2. > 0. ? v : pow((x - x_m - Dx/2.),4.)*pow((x - x_m + Dx/2.),4.) 
	 * pow((x - x_m),3.) * 2500. / pow(Dx/2.,9.) + v;
#endif

    }
  break;
  }
  
case 1:
 {register int g_ind1; 
 FLOAT x;

  for (g_ind1 = ni_1; g_ind1 < nf_1; ++g_ind1) {

    x= xi + (FLOAT)g_ind1*one_dN1;
    
#ifdef ADV
        	(*y_a_ptr).u[U][g_ind1] = fabs(x) > 0.5 ? 0.0 : pow((x-0.5)*(x+0.5),8) * 65536.0;  //normalization factor = 2**16
			(*y_a_ptr).u[U][g_ind1] += fabs(x-0.3) > 0.4 ? 0.0 : pow((x-0.7)*(x+0.1),8) * 2328306.43654 * 0.7; //normalization factor = 2.5**16
#endif	
    }
break;

}

case 2:
 {register int g_ind1; 
 FLOAT x;

  for (g_ind1 = ni_1; g_ind1 < nf_1; ++g_ind1) {

    x= xi + (FLOAT)g_ind1*one_dN1;
    
#ifdef ADV    
        	(*y_a_ptr).u[U][g_ind1] = fabs(x) > 0.5 ? 0.0 : pow((x-0.5)*(x+0.5),8) * 65536.0;
        	FLOAT y = x < 0.0 ? x+2.0 : x;
			(*y_a_ptr).u[U][g_ind1] += fabs(y-1.0) > 0.2 ? 0.0 : pow((y-1.2)*(y-0.8),8) * 152587890625 * 0.7; //normalization factor = 5**16
#endif			
    }
break;

}
}

// Auxiliary fields
 
  printf("<LI>Inidat finished </br></LI>\n"); 

}
