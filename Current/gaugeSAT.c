x = xi + (FLOAT)grid_ind1*one_dN1;
t = (*derivs_ptr).time; 
    
AP = A*PI; 
    
cosphi = cos(PI*(x-t));
sinphi = sin(PI*(x-t));
pisinphi = PI*sinphi;
Ac = AP*cosphi;
As = AP*pisinphi;

a_h = exp(A*sinphi/2.);

FLOAT X11, X12, X13, X21, X22, X23, X31, X32, X33;
FLOAT Xinv11, Xinv12, Xinv13, Xinv21, Xinv22, Xinv23, Xinv31, Xinv32, Xinv33;

FLOAT Amod11, Amod12, Amod13, Amod21, Amod22, Amod23, Amod31, Amod32, Amod33;

FLOAT mlam1 = 0.0*nx > 0.0 ? 0.0 : 0.0;
FLOAT mlam2 = (-1.0)*nx > 0.0 ? (-1.0) : 0.0;
FLOAT mlam3 = (1.0)*nx > 0.0 ? (1.0) : 0.0;

X11 = 0.0;
X12 = -1/a_h;
X13 = 0.0;
X21 = -a_h;
X22 = 0.0;
X23 = 0.0;
X31 = 0.0;
X32 = -2.0*a_h;
X33 = 0.0;

Xinv11 = -2.0*a_h*a_h;
Xinv12 = 0.0;
Xinv13 = 1.0;
Xinv21 = -0.5*a_h;
Xinv22 = 0.5;
Xinv23 = 0.0;
Xinv31 = 0.5*a_h;
Xinv32 = 0.5;
Xinv33 = 0.0;


Amod11 = X11 * mlam1 * Xinv11 + X12 * mlam2 * Xinv21 + X13 * mlam3 * Xinv31;
Amod12 = X11 * mlam1 * Xinv12 + X12 * mlam2 * Xinv22 + X13 * mlam3 * Xinv32;
Amod13 = X11 * mlam1 * Xinv13 + X12 * mlam2 * Xinv23 + X13 * mlam3 * Xinv33;
Amod21 = X21 * mlam1 * Xinv11 + X22 * mlam2 * Xinv21 + X23 * mlam3 * Xinv31;
Amod22 = X21 * mlam1 * Xinv12 + X22 * mlam2 * Xinv22 + X23 * mlam3 * Xinv32;
Amod23 = X21 * mlam1 * Xinv13 + X22 * mlam2 * Xinv23 + X23 * mlam3 * Xinv33;
Amod31 = X31 * mlam1 * Xinv11 + X32 * mlam2 * Xinv21 + X33 * mlam3 * Xinv31;
Amod32 = X31 * mlam1 * Xinv12 + X32 * mlam2 * Xinv22 + X33 * mlam3 * Xinv32;
Amod33 = X31 * mlam1 * Xinv13 + X32 * mlam2 * Xinv23 + X33 * mlam3 * Xinv33;

/*
 *MAL IMPLEMENTADO*
Amod11 = v*v/s*(gamma-1.0)*1.5+v;
Amod12 = v*(gamma-1.0)*(v/s*1.5+2.0) + s;
Amod13 = v*(gamma-1.0)*(v/s*1.5-2.0) + s;
Amod21 = 0.5*v*(1.0-v/s*(gamma-1.0)*1.5);
Amod22 = 0.5*v*(1.0-v/s*(gamma-1.0)*1.5-2.0*(gamma-1.0)) + v;
Amod23 = 0.5*v*(1.0-v/s*(gamma-1.0)*1.5 + 2.0*(gamma-1.0)) - s;
Amod31 = -0.5*v*(1.0+v/s*(gamma-1.0)*1.5);
Amod32 = -0.5*v*(1.0 + v/s * (gamma-1.0)*1.5 + 2.0*(gamma-1.0)) - s;
Amod33 = -0.5*v*(1.0 + (gamma-1.0)*(v/s*1.5-2.0)) + v;
*/

FLOAT SATAx  = -nx*L*(Amod11 * du1  +  Amod12 * du2);
FLOAT SATK   = -nx*L*(Amod21 * du1  +  Amod22 * du2);
FLOAT SATd   = -nx*L*(Amod31 * du1  +  Amod32 * du2);
