#define _POSIX_C_SOURCE 200112L
#include <stdlib.h>
#include <sys/time.h>
#include <fenv.h>
	

#include "first_macro_1d.h"  /* Where global parameters are defined */
#include "structs_1d.h"      /* Where structures are defined */
//#include "derivs_1d.h"       /* Where derivatives functions are defined */
//#include "gen_1d.h"
#include "globals.h"
#include "inidat.h"
#include "input.h"
#include "equation.h" 	
#include "integ.h"
#include "rkc.h"
#include "adisco_1d.h" 

// 4M hugepage boundary
#define HUGEPAGE_SIZE (1 << 22)

/***********************   Global variables   ****************************/
struct globals globals;

/***********************   Helper functions   ******************************/

/* Allocate a set of fields at once in huge pages if possible */
void alloc_field_data(size_t nfields, size_t field_elems, FLOAT ** data) {
    FLOAT * memblock;

    size_t alloc_size = nfields * field_elems * sizeof(FLOAT);
    // Request a hugepage-aligned chunk of memory
    if (posix_memalign((void **) &memblock, HUGEPAGE_SIZE, alloc_size) != 0) {
        fprintf(stderr, "out of memory in posix_memaling\n");
        exit(1);
    }

#ifdef MADV_HUGEPAGE
    // Give the OS a hint
    madvise(memblock, alloc_size, MADV_HUGEPAGE);
#endif

    // Copy pointers to each field
    for (size_t i = 0; i < nfields; ++i) {
        data[i] = &memblock[i * field_elems];
    }
}

/* Free a set of fields allocated with alloc_field_data */
void free_field_data(FLOAT ** data) {
    free(*data);
}

void * safe_malloc(size_t size) {
    void * rv = malloc(size);
    if (rv == NULL) {
        fprintf(stderr, "out of memory in safe_malloc\n");
        exit(1);
    }
    return rv;
}

/***********************   Global variables   ****************************/


/*
void norm_L2(struct GRID_PAR *grid_1d_ptr, 
		struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *fields_ptr);
		
void norm_Energy(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *fields_ptr);

*/

int main() {

  /* variable declarations */

  struct GRID_PAR grd;
  struct GRID_PAR *grd_ptr = &grd;
  FLOAT h;
  struct field_array y;


  /*   Ploting names */

  struct PLOT_PAR plot; 
  struct PLOT_PAR *plot_ptr = &plot;

  /*   Initial data parameters */

  struct INI_PAR init_parameters;
  struct INI_PAR *init_par_ptr = &init_parameters;
  
  
  /*   Function parameters  */


  struct FUNCTION_PAR equation_parameters;
  struct FUNCTION_PAR *equation_par_ptr = &equation_parameters;

  /* Parameters coming from first_macro */


#ifdef FILE_INPUT
  FILE *file_data_ptr;
#endif

// For measuring time 

struct timeval start, stop, end;
double tiempo;






  /* Get data from web page or data file */
 



INPUT_FUNCTION(grd_ptr, equation_par_ptr, 
		 init_par_ptr, plot_ptr);

 printf("out of input function\n");

file_data_ptr = plot_ptr->input_data_file_ptr;
PRINT_MACRO_VALUE(RKX)
PRINT_MACRO_VALUE(DERIV)
PRINT_MACRO_VALUE(FF)
#ifdef IMEX
PRINT_MACRO_VALUE(FF)
PRINT_MACRO_VALUE(FS)
PRINT_MACRO_VALUE(FI)
#else
PRINT_MACRO_VALUE(FF)
#endif //IMEX
PRINT_MACRO_VALUE(ADISCO)
#ifdef DISSIPATION
PRINT_MACRO_VALUE(DISS)
#endif

    /* ------------------------------------ Allocate memory ------------------------------------ */

    /* Allocation #1:------->  auxiliary fields for rkc */

#ifdef IMEX
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_tempF2.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_tempF3.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_tempF4.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_tempS1.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_tempS2.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_tempS3.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.v_temp0.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.v_temp1.u);

#else
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_temp.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_sum.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.v_temp.u);
#endif    
    
    /* Allocation #2: ------> The fields */
    alloc_field_data(N_FIELDS, grd.n_grid_pts, y.u);

    /* Allocation #3:-------> their derivatives (used in evaluating the function) */
    alloc_field_data(N_DERIVS, grd.n_grid_pts, globals.dfields.du);

    /* Allocation #4:--------->  other auxiliary fields */
    alloc_field_data(N_AUX, grd.n_grid_pts, globals.auxfields.u_aux);

    /* Allocation #5 :---------> plot vector memory */
//    plot_ptr->plot_field = (double *)safe_malloc((plot_ptr->grid_plot_pts_1) * sizeof(double));

//exit(0);

#ifdef SDF
    plot_ptr->plot_field = safe_malloc((plot_ptr->grid_plot_pts_1) * sizeof(double));
#endif

printf("grid_plot_pts_1 = %d \n", plot_ptr->grid_plot_pts_1);

#ifdef PYGRAPH
	plot_ptr->plot_field_pygraph = safe_malloc((plot_ptr->grid_plot_pts_1) * sizeof(float)*2);
#endif	
    


/* Array of names for plots */

#ifdef AVD
 sprintf(plot_ptr->name[0],"U");
 //sprintf(plot_ptr->name[1],"A");

 sprintf(plot_ptr->window_name[0], "%s_U_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);
 //sprintf(plot_ptr->window_name[1], "%s_A_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);

  plot_ptr->initial_x= grd.initial_x;
  plot_ptr->final_x = grd.final_x;


  plot_ptr->n_plots = N_PLOTS;

/* Relation between fields and plot names */

  plot_ptr->pointers[0] = U;  
  //plot_ptr->pointers[1] = A;
  
#endif

#ifdef WAVE
 sprintf(plot_ptr->name[0],"PHI");
 sprintf(plot_ptr->name[1],"FU0");
 sprintf(plot_ptr->name[2],"FD1");

 sprintf(plot_ptr->window_name[0], "%s_PHI_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);
 sprintf(plot_ptr->window_name[1], "%s_FU0_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);
 sprintf(plot_ptr->window_name[2], "%s_FD1_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);

  plot_ptr->initial_x= grd.initial_x;
  plot_ptr->final_x = grd.final_x;


  plot_ptr->n_plots = N_PLOTS;

/* Relation between fields and plot names */

  plot_ptr->pointers[0] = PHI;  
  plot_ptr->pointers[1] = FU0;
  plot_ptr->pointers[2] = FD1;
  
#endif

#ifdef DYN

 sprintf(plot_ptr->name[0],"U");
 sprintf(plot_ptr->name[1],"A");

 sprintf(plot_ptr->window_name[0], "%s_U_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);
 sprintf(plot_ptr->window_name[1], "%s_A_%d" ,plot_ptr->output_file_name,grd.n_grid_pts);

  plot_ptr->initial_x= grd.initial_x;
  plot_ptr->final_x = grd.final_x;


  plot_ptr->n_plots = N_PLOTS;

/* Relation between fields and plot names */

  plot_ptr->pointers[0] = U;  
  plot_ptr->pointers[1] = A;

#endif 
  
  /* Factor between gridpts and gridplotpts */
 
  //plot_ptr->grid_plot_factor_1 = grid_plot_factor_1; 
  /* Initial/Final value for time coordinate */

    plot_ptr->initial_time = grd.initial_time;       
    plot_ptr->final_time = grd.final_time;   
    plot_ptr->time_slice = 0;
  

  /* Open output file (some times used only for compatibility) */
printf("opening file\n");



//			plot_ptr = ADISCO('O',  &plot, &grd, &y); 

	plot_ptr = adisco_aschi_1d('O',  &plot, &grd, &y);
	plot_ptr = adisco_energy_1d('O',  &plot, &grd, &y);
//  ADISCO_POINT('O', &plot, &grd, &y);

  /*     creates initial data                            */

inidat(&y,grd_ptr,&init_parameters);

/* write initial data to file                          */ 

/* plot data */
      
	
 
		//#ifdef SDF

	    //plot_ptr = ADISCO('P',  &plot, &grd, &y); 

		//#endif
	
//    ADISCO_POINT('P',  &plot, &grd, &y); 
	
		plot_ptr = adisco_aschi_1d('P',  &plot, &grd, &y);

		plot_ptr = adisco_pygraph_1d('W',  &plot, &grd, &y);

		adisco_energy_1d('P',  &plot, &grd, &y);
  /*     creates potential function */

		// exit(0); 

/* inipot(pot_ptr,&pot_parameters, &grd, plot_ptr); */




  /* makes initial time-interval */

 
  h = grd.time_step; // to be used in the equations.

  /* sends input data to file / browser */

fprintf(OUT_PUT,"<li> Total number of Time Steps = %f </br>\n",(double)(grd.data_steps*grd.int_steps));

fprintf(OUT_PUT,"<li> Number of Time Steps per unit time = %f </br>\n",1.0/h);

fprintf(OUT_PUT,"<li> Time_Step / Space_Step_x = (h/(xf-xi)*(n_grid_pts-1)) = %f </br>\n",h*(double)grd.n_grid_pts/(grd.final_x-grd.initial_x));
fprintf(OUT_PUT,"</ul>%c</br>",10);

fflush(stdout);

/* send input data to the screen */

#ifdef FILE_INPUT
printf("Total number of Time Steps = %f \n",(double)(grd.data_steps*grd.int_steps));

printf("Number of Time Steps per unit time = %f , dt =%f \n",1.0/h, h);

printf("Time_Step / Space_Step_x= (h/(xf-xi)*n_grid_pts) = %f \n",h*(double)grd.n_grid_pts/(grd.final_x-grd.initial_x));
printf("\n");

fflush(stdout);
#endif

  	// norm_L2(&grd, &y);

  /* Take data_steps */

  {long int k_outer;
  for (k_outer=1; k_outer<= grd.data_steps; k_outer++) {
/*       printf("h = %f\n",h);  */
/*       printf("time = %f\n",y.a.time); */

#ifdef IMEX
integ(&y,grd_ptr,equation_par_ptr,FF,FS,FI,RKX); 
#else
integ(&y,grd_ptr,equation_par_ptr,FF,RKX); 
#endif //IMEX

//FLOAT energia;
//INTERNALPRODUCT(grd_ptr, y.u[U], y.u[U], &energia);

//printf("energy = %.20lf\n", energia);

/* 	printf("time after integ in main = %f",y.a.time); */
/* printf("Out of integ \n");  */


//      printf("...");
      fflush(stdout);
      /* Do pointwise output */
      //ADISCO_POINT('P',  &plot, &grd, &y); 
      /* Do 1d output */
      
            if ((k_outer%plot.grid_plot_factor_1)==0){
			    #ifdef SDF
			  		  plot_ptr = ADISCO('P', &plot, &grd, &y);  
			    #endif
  //printf("a punto de entrar\n");
		        plot_ptr->time_slice = k_outer;
		        plot_ptr = adisco_pygraph_1d('A',  &plot, &grd, &y);
		        
				adisco_energy_1d('P',  &plot, &grd, &y);
				
  //printf("salgo\n");
				
		//  norm_L2(&grd, &y);
            }

	 


/* printf("�</br>\n");  */ 
printf("r"); 
/* printf("%c",7); */ 
fflush(stdout);
  }



  }
	
//	 plot_ptr = adisco_aschi_1d('P', &plot, &grd, &y); // printing the last value in aschi at .dat files.

plot_ptr = ADISCO('P', &plot, &grd, &y);

fprintf(OUT_PUT,"<ul>%c</br>",10);
fprintf(OUT_PUT,"<li> Execution time = %u secs. ", (unsigned)(clock()/CLOCKS_PER_SEC));
fprintf(OUT_PUT,"</ul>%c</br>",10);

#ifdef FILE_INPUT
printf("\n");
printf("Execution time = %u secs. ", (unsigned)(clock()/CLOCKS_PER_SEC));
printf("\n");
#endif

/* close output file */
    plot_ptr = ADISCO('C',  &plot, &grd, &y); 
    
	adisco_energy_1d('C',  &plot, &grd, &y);
	
//	plot_ptr = adisco_aschi_1d('C',  &plot, &grd, &y); 

#ifdef FILE_INPUT
  fclose(plot_ptr->input_data_file_ptr);
#endif
printf("%c",7);
printf("finishing \n");
return(0);
}

/*
void norm_L2(struct GRID_PAR *grid_1d_ptr,
		union fields *fields_ptr)
		{
		    int ni_1 = (*grid_1d_ptr).start_grid_1; 
			int nf_1 = (*grid_1d_ptr).final_grid_1; 

			
			FLOAT xi = (*grid_1d_ptr).initial_x;
			FLOAT xf = (*grid_1d_ptr).final_x;
			
			FLOAT N=0.0;
			
			int i;

DERIV(grid_1d_ptr,(struct field *)&(*fields_ptr).a.u[PHI][0],
         (struct field *)&dfields.du[PHI_X][0]);

#ifdef PERIODIC

			for(i=ni_1; i < nf_1; i++){
#ifndef ADV
				N = N + (*fields_ptr).a.u[PHI_T][i]*(*fields_ptr).a.u[PHI_T][i] + dfields.du[PHI_X][i]*dfields.du[PHI_X][i];
#else
				N = N + (*fields_ptr).a.u[PHI][i]*(*fields_ptr).a.u[PHI][i];
#endif
			}
#else			
			
			for(i=ni_1+1; i < nf_1-1; i++){
#ifndef ADV
				N = N + (*fields_ptr).a.u[PHI_T][i]*(*fields_ptr).a.u[PHI_T][i] + dfields.du[PHI_X][i]*dfields.du[PHI_X][i];
#else
				N = N + (*fields_ptr).a.u[PHI][i]*(*fields_ptr).a.u[PHI][i];
#endif
			}
#ifndef ADV			
			i = ni_1;
			N = N + 0.5*((*fields_ptr).a.u[PHI_T][i]*(*fields_ptr).a.u[PHI_T][i] + dfields.du[PHI_X][i]*dfields.du[PHI_X][i]);
			i = nf_1-1;
			N = N + 0.5*((*fields_ptr).a.u[PHI_T][i]*(*fields_ptr).a.u[PHI_T][i] + dfields.du[PHI_X][i]*dfields.du[PHI_X][i]);
#else
			i = ni_1;
			N = N + 0.5*((*fields_ptr).a.u[PHI][i]*(*fields_ptr).a.u[PHI][i]);
			i = nf_1-1;
			N = N + 0.5*((*fields_ptr).a.u[PHI][i]*(*fields_ptr).a.u[PHI][i]);
#endif
			
			nf_1 = nf_1-1; // to make the integral correct
  
#endif
			

			
		printf("Time = %f, L2 norm = %f \n",(*fields_ptr).a.time, N*(xf-xi)/(double)(nf_1 - ni_1));
			
		}
*/
