
/*********************************************************************
*                                                                    *
* This is the Header file of gen.c a general solver for evolutinary  *
* PDE's which uses standard ODE solvers and Spectral derivatives.    *
*                                                                    *
* Basic definitions include the number of grid points, the number of *
* variables.                                                         *
*                                                                    *
*--------------------------------------------------------------------*
*                                                                    *
* Basic structure is of a field:                                     *
*       struc field_array                                            *
* It holds the array pointing to each gridpoint and the time         *
* There will be also an struc bigvector which points to the whole    *
* array structure (a Union)                                          *
*                                                                    *
*--------------------------------------------------------------------*
*                                                                    *
* Functions in different files:                                      *
*                                                                    *
*    intdat -- provides the initial data (it contains physical parm. *
*    odeint -- integrates ordinary diff. equations dy/dt=F(y,t).     *
*    F      -- routine called by odeint which computes the value of  *
*              the right hand side of the above equation. Here are   *
*              physical parameters.                                  *
*    derivs -- computes derivatives using FFT, the relevant subrouti *
*              nes are contained there.                              *
*    adisco -- Send information to the files                         *
*                                                                    *
*                                                                    *
*********************************************************************/

#ifdef GEN_1D_H
#else
#define GEN_1D_H


#include "first_macro_1d.h"  /* Where global parameters are defined */
#include "structs_1d.h"      /* Where structures are defined */
#include "derivs_1d.h"       /* Where derivatives functions are defined */







struct w2_ini_par_1d {
  struct FUNCTION_PAR *function_par_ptr;
  struct GRID_PAR *grid_ptr;
/* Initial data is given by:                                    */
/*       U[0] = PHI = (a0*sin(k_a_10 x + k_a_20 y + shift_a0)   */
/*       + c_0*cos(k_c_10 x + k_c_20 y + shift_c0))*            */
/*       b_0*exp(-sigma_b0*((x-c0_1)^2+(y-c0_2)^2)             */
/* U[1] = dPHI/dt = (a1*sin(k_a_11 x + k_a_21 y + shift_a1)     */
/*       + c_1*cos(k_c_11 x + k_c_21 y + shift_c1))*            */
/*       b_1*exp(-sigma_b1*((x-c1_1)^2+(y-c1_2)^2+)             */
/*       + v1*U[0]_x + v2*U[0]_y                               */

  FLOAT a0;                
  FLOAT k_a_10; 
  FLOAT shift_a0;         

  FLOAT a1;              
  FLOAT k_a_11;   
  FLOAT shift_a1;     

  FLOAT c0;             
  FLOAT k_c_10;
  FLOAT shift_c0;     

  FLOAT c1;            
  FLOAT k_c_11;  
  FLOAT shift_c1;       
  

  FLOAT b0;           
  FLOAT sigma_b0;
  FLOAT c0_1;

  FLOAT b1;               
  FLOAT sigma_b1;
  FLOAT c1_1;

    FLOAT v1;
};

struct w2_par_1d { 
  struct GRID_PAR *grid_ptr;
  FLOAT c;      /* mass of field */
  FLOAT s; 
  FLOAT sigma;   /* dissipation parameter */
  FLOAT R10;     /* Boundary condition at x=0, m = R0*p */
  FLOAT R11;     /* Boundary condition at x=1, p = R1*m */


  /* normals to fases, first digit = coord. second = value */

  FLOAT nx_10;
  FLOAT nx_11;

};


/* ---------------------------> FUNCTIONS <------------------------ */

/*********************************************************************
*                                                                    *
* inidat -- provides initial data                                    *
*                                                                    *
* Parameters:                                                        *
*       y_a_ptr -- pointer where to write the initial data          *
*                                                                    *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/

extern void inidat(union fields *y_a_ptr, 
		   struct GRID_PAR *grid_1d_ptr,
		   struct INI_PAR *ini_par_ptr
		   );



/*********************************************************************
*                                                                    *
* integ  -- integrates dy/dt = f(y,t) between two time points        *
*                                                                    *
* Parameters:                                                        *
*   y_b_ptr     -- pointer to the initial data in field_vector struct*
*       h       -- FLOAT given the time step zice                   *
*   int_steps   -- number of steps between data savings              *
*                  (so total # of steps is data_steps x int_steps    *
*       F       -- pointer to fuction which evaluates the f(y,t)     *
*       RKX     -- pointer to runge-kutta (or any other) engine      *
*                                                                    *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/
#ifdef IMAX

extern void integ(union fields *y_b_ptr,  
	   struct GRID_PAR *grid_ptr,
	   struct FUNCTION_PAR *equation_par,
	   void (* FF)(FF_ARG_DUMMY),void (* FS)(FF_ARG_DUMMY),void (* FI)(FF_ARG_DUMMY), 
	   void (* RKX)(union fields *, 
			union fields *,
			struct GRID_PAR *,
			struct FUNCTION_PAR *, 
			void (* )(struct GRID_PAR *,
				  union fields *, 
				  union fields *,
				  struct FUNCTION_PAR *),
			void (* )(struct GRID_PAR *,
				  union fields *, 
				  union fields *,
				  struct FUNCTION_PAR *),
			void (* )(struct GRID_PAR *,
				  union fields *, 
				  union fields *,
				  struct FUNCTION_PAR *)
			)
	   );
#else
extern void integ(union fields *y_b_ptr,
		  struct GRID_PAR *grid_ptr,
		  struct FUNCTION_PAR *function_par,
		  void (* FF)(FF_ARG_DUMMY), 
		  void (* RKX)(union fields *, 
			       union fields *, 
			       struct GRID_PAR *,
			       struct FUNCTION_PAR *, 
			       void (* )(struct GRID_PAR *,
					 union fields *, 
					 union fields *,
					 struct FUNCTION_PAR *)
			       )
		  );
#endif

/*********************************************************************
*                                                                    *
* FF -- evaluates the function f(y,t), lots of physical imput on it  *
*                                                                    *
* Parameters:                                                        *
*       fields -- pointer to field_vector from where to extract (y,t)*
*       derivs -- pointer to field_vector where to put derivatives   *
*       function_par -- pointer to equation parameters               *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/

extern void FF(struct GRID_PAR *grid_1d_ptr,
	       union fields *fields, 
	       union fields *derivs, 
	       struct FUNCTION_PAR *function_par
	       );

#ifdef IMAX 
// The complete RHS
extern void FS(struct GRID_PAR *grid_1d_ptr,
	       union fields *fields, 
	       union fields *derivs, 
	       struct FUNCTION_PAR *function_par
	       );
// The implict inversion
extern void FI(struct GRID_PAR *grid_1d_ptr,
	       union fields *fields, 
	       union fields *derivs, 
	       struct FUNCTION_PAR *function_par
	       );
#endif // IMAX








/*********************************************************************
*                                                                    *
* adisco -- sends data to files                                      *
*                                                                    *
* Parameters:                                                        *
*       inst   -- char with instructions ("OPEN", "PUT", or "CLOSE") *
*       fields -- pointer to union fields with data                  *
*                                                                    *
* Returns: execution status                                          *
* Location: file adisco.c (it contains all of them)                  *
*                                                                    *
*********************************************************************/



extern struct PLOT_PAR *adisco_sv_1d(char inst, 
				   struct PLOT_PAR *plot_ptr, 
				   struct GRID_PAR *gri, 
				   union fields *fields);

extern struct PLOT_PAR *adisco_fsv_1d(char inst, 
				   struct PLOT_PAR *plot_ptr, 
				   struct GRID_PAR *gri, 
				   union fields *fields);

extern struct PLOT_PAR *adisco_sdf_1d(char inst, 
				   struct PLOT_PAR *plot_ptr, 
				   struct GRID_PAR *gri, 
				   union fields *fields);



extern struct PLOT_PAR *adisco_txt_point_1d(char inst,
			     struct PLOT_PAR *plot_ptr,
			     struct GRID_PAR *gri,
			     union fields *fields);

extern struct PLOT_PAR *adisco_aschi_1d(char inst,
								 struct PLOT_PAR *plot_ptr,
								 struct GRID_PAR *grid,
								 union fields *fields);


extern struct PLOT_PAR *adisco_energy_1d(char inst,
								 struct PLOT_PAR *plot_ptr,
								 struct GRID_PAR *grid,
								 union fields *fields);


/*********************************************************************
*                                                                    *
* plot_prep   prepares plotting structures to be sent to files       *
*                                                                    *
*********************************************************************/


extern void plot_prep(struct PLOT_PAR *plot_ptr, 
		      struct FUNCTION_PAR *function_par, 
		      union fields *y_ptr
		      );



/*********************************************************************
*                                                                    *
*  rk3 -- integrates    dy/dt = f(y,t)     using runge-kutta         *
*                                          third order               *                            
* Parameters:                                                        *
*      v_init_ptr     -- pointer to where the y initial is           *
*      v_fina_ptr     -- pointer to where the y final is             *
*      h              -- time step                                   *
*      function_par   -- pointer to parameters to pass to FF         *
*      FF              -- pointer to function which computes f(y,t)  *
*                                                                    *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/

extern void rk3(union fields *v_init_ptr, 
		union fields *v_fina_ptr, 
		struct GRID_PAR *grid_1d_ptr,
		struct FUNCTION_PAR *function_par,
		void (* FF)(FF_ARG_DUMMY));

/*********************************************************************
*                                                                    *
*  rk4 -- integrates    dy/dt = f(y,t)     using runge-kutta         *
*                                          fourth order              *
* Parameters:                                                        *
*      v_init_ptr     -- pointer to where the y initial is           *
*      v_fina_ptr     -- pointer to where the y final is             *
*      h              -- time step                                   *
*      function_par   -- pointer to parameters to pass to FF         *
*      FF              -- pointer to function which computes f(y,t)  *
*                                                                    *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/

extern void rk4(union fields *v_init_ptr, 
		union fields *v_fina_ptr, 
		struct GRID_PAR *grid_1d_ptr,
		struct FUNCTION_PAR *function_par,
		void (* FF)(FF_ARG_DUMMY));


/*********************************************************************
*                                                                    *
*  tvd3 -- integrates    dy/dt = f(y,t)     total variation          *
*                                    diminishing runge-kutta         *
*                                          third order               *
* Parameters:                                                        *
*      v_init_ptr     -- pointer to where the y initial is           *
*      v_fina_ptr     -- pointer to where the y final is             *
*      h              -- time step                                   *
*      function_par   -- pointer to parameters to pass to FF         *
*      FF              -- pointer to function which computes f(y,t)  *
*                                                                    *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/


/************************************************************************/
/* The scheme is:                                                       */
/* u^1 = u^n + h*F(u^n,t)                                               */
/* u^2 = 3u^n/4 + u^1/4 + h/4*F(u^1,t+h)                                */
/* u^(n+1) = u^n/3 + 2*u^2/3 * 2h/3*F(u^2,t+h/2)                        */
/*                                                                      */
/* ver paper de Luis del 2008                                           */
/************************************************************************/

extern void tvd3(union fields *v_init_ptr, 
		union fields *v_fina_ptr, 
		struct GRID_PAR *grid_1d_ptr,
		struct FUNCTION_PAR *function_par,
		void (* FF)(FF_ARG_DUMMY));

extern void imexrkssp3(union fields *v_init_ptr, 
	 union fields *v_fina_ptr, 
	 struct GRID_PAR *grid_ptr,
	 struct FUNCTION_PAR *equation_par,
	 void (* FF)(FF_ARG_DUMMY),void (* FS)(FF_ARG_DUMMY),void (* FI)(FF_ARG_DUMMY) ); 



extern void imexrkL343(union fields *v_init_ptr, 
	 union fields *v_fina_ptr, 
	 struct GRID_PAR *grid_ptr,
	 struct FUNCTION_PAR *equation_par,
	 void (* FF)(FF_ARG_DUMMY),void (* FS)(FF_ARG_DUMMY),void (* FI)(FF_ARG_DUMMY) );    


     
/* ----------------------- Web Data Functions ------------------------------*/
     
/****************************************************************************
 * INPUT_FUNCTION -- This function inputs data to the program which is      *
 * generated via a wave page, the input data structures are given above     *
 *                                                                          *
 ****************************************************************************/
extern  void INPUT_FUNCTION(struct GRID_PAR *grid_parameters, 
			    struct FUNCTION_PAR *function_parameters, 
			    struct INI_PAR *ini_parameters, 
			    struct PLOT_PAR *plot_parameters);



/****************************************************************************
 * Get values from an entry array and asign them to pointer given           *
 *                                                                          *
 * m -- pointer to int giving the number of entries                         *  
 * name[10] -- name of variable as given in entry                           *
 * entries  -- pointer to entry from where to obtain the pairs name/value   *
 * Return value of name                                                     *
 ***************************************************************************/

extern FLOAT get_value_double(char name[MAX_ENTRY], 
			      struct entries *entries,
			      struct PLOT_PAR *plot_par
			      );

extern int get_value_int(char name[MAX_ENTRY], 
			 struct entries *entries,
			 struct PLOT_PAR *plot_par
			 );

extern char *get_value_char(char name[MAX_ENTRY], 
			    struct entries *entries,
			    struct PLOT_PAR *plot_par
			    );

extern void parse_stain(struct entries *entriesp);

     
extern void parse_filedata(struct entries *entriesp);



/****************************************************************************
 * This are utilities functions to handle data given via web                *
 *                                                                          *
 ****************************************************************************/
extern  char *makeword(char *line, char stop);
extern  char *fmakeword(FILE *f, char stop, int *len);
extern  char x2c(char *what);
extern  void unescape_url(char *url);
extern  void plustospace(char *str);


/*************************************************************************/

/**********************   Global variables  ******************************/


/* ---------------------------  rkc -------------------------------------*/

#ifdef IMAX
//extern union fields dv_tempF1;        /* derivative temporal value */
extern union fields dv_tempF2;     
extern union fields dv_tempF3; 
extern union fields dv_tempF4;  
extern union fields dv_tempS1; 
extern union fields dv_tempS2;  
extern union fields dv_tempS3;   
//extern union fields dv_tempS4;
extern union fields dv_sum; 
extern union fields v_temp0; 
extern union fields v_temp1;  /* intermediate value of v    */
 
//extern union fields *dv_tempF1_ptr;        /* derivative temporal value */
extern union fields *dv_tempF2_ptr;     
extern union fields *dv_tempF3_ptr; 
extern union fields *dv_tempF4_ptr;  
extern union fields *dv_tempS1_ptr; 
extern union fields *dv_tempS2_ptr;  
extern union fields *dv_tempS3_ptr;  
//extern union fields *dv_tempS4_ptr; 
extern union fields *v_temp0_ptr; 
extern union fields *v_temp1_ptr;  /* intermediate value of v    */

#else

extern union fields dv_temp;        /* derivative temporal value         */
extern union fields dv_sum;         /* sum values of deriv               */
extern union fields v_temp;         /* intermediate value of v           */


extern union fields *dv_temp_ptr;        /* derivative temporal value        */
extern union fields *dv_sum_ptr;         /* sum          values of deriv     */
extern union fields *v_temp_ptr;         /* intermediate value of v          */

#endif //IMAX

/* ---------------------------- FF --------------------------------------*/
    
               /* temporal space derivative values */

extern struct field_array_derivs dfields;     
 
extern struct field_array_aux aux_fields;

/********************************************************************/



extern void norm_L2(struct GRID_PAR *grid_1d_ptr,
		union fields *fields_ptr);


#ifdef LOOPING

extern int p; // for looping
#endif // LOOPING


#endif
